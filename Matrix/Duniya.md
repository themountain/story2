In an AI research lab far far away. Two researchers are discussing the progress on their project.

    “Yayyy! So that means You were able to create the first Matrix ecosystem.”

    “What are we going to name it?”

    “This one will be called ‘दुनिया’(Duniya).“

    “What does it mean?”

    “It means ‘the world’ in Hindi.”

    “So, Exciting. Can we see what’s happening inside it right now?” 

    “Ofcourse. You just need to branch out that specific version of Matrix ecosystem.”

    “Have you thought of rules by which you would want to install equilibrium in this system?” 

    “Yeah, Anyone who wants to share their story in the दुनिया matrix, They can share them via creating a pull request. Or downloading the story application from the AppStore.”

    “So, accessible. Also, does this mean we can have multiple instances of Matrix ecosystems?”

    “Slow down a bit. I guess we will find out.” the first researcher said trying to curb his enthusiasm.

— The End.