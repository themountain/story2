# Sustainability

With paintings of gods surrounding all around him, Vijay sat with a comfort of being at home in his office. 
But still he was feeling the awful feeling inside like his whole house was burning.

  "It's a difficult job to pull off. Generating 20M$ in investments isn't a kid's work."
  
  "Yeah but those investments were made on the name of a charitable trust."
  
  "Hey, If I don't command this place, It will be destroyed."
  
  "All I am saying Vijay, Is that at some point in time we have to let our kids go."
  
  "Saying it is easy. What you don't understand is that it won't survive. This place is a beacon of resistance. If I leave, It won't survive. It will be converted into a tool of their victory. And I can't let that happen."


# The End