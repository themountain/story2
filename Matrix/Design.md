    "What can I get for you?"

    "A coffee please." said the Customer.

    "Sure. Which one?

    1. Latte
    2. Espresso
    3. Americano
    4. Macchiato
    5. Cortado
    6. Cappuccino
    7. Mocha
    8. Cafe Latte
    9. Vienna
    10. Iced Coffee." said the server.

    "One Americano please."

    "Sure. Which one?

    1. Small
    2. Medium
    3. Large"

    "Do you have anything in between small and medium?"

    "Apologies. That is not how my program is designed."

-- The End.