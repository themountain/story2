    "The AI is nothing but a machine trained on your information." The TV anchor roared like the news was something out of a pacifist utopia.

    Jim unsettled after hearing this news, turns off the news application. He spent his next 2 hours, surfing the net. Trying to find a way to cancel his subscription. Frustrated him at the end CALLS the customer service:

    "What do you mean you won't compensate for my data? I am paying almost 10% of my salary to this."

    "But it is a premium service. Because we run it on our servers."

    "Yes. But the 'thing' that I am paying for... Would not have existed if I had not shared my data."

    "Well okay. We can develop a feature that lets you store your data. And sell it to us in return for money."

---------*

        "Trrrrrrrrrrrrrrrrrrrrrrrr.....Trrrrrrrrrrrrrrrrr" rang the alarm clock.

-- The End.