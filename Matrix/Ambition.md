    "Why are you pursuing such frivolous projects?"

    "What do you mean?"

    "You used to earn 80K at your job. This 'pet' project of yours would have the ROI of 5K per annum if you are lucky enough to find the right market."

    "You don't understand scale as I do father. You have never looked at it from where I stand. I am looking forward to cover this whole globe and then some more with this 'pet' project of mine. And it is just the start."

-- The End.