    "Because it's better to die on one's feet than to live on one's knees. I guess you have heard that saying before."

    "Yes, I certainly have. But I'm afraid you have it backwards. It is better to live on one's feet than die on one's knees. That is the way the saying goes."

    "Are you sure? It seems to make more sense my way."

    "Well, You will need to know the Catch to understand it my way. Catch-22."

-- The End.