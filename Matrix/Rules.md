    "If you look at the details a bit in detail, You will find this universe to be in form of a dictionary. Which is created from pairs of Filename -> Story. You will see that some files have the word mentioned inside the story. While others don't. But the filename is connected to the story through some metaphorical meaning.

    Which gives this universe a structure of a metaphorical dictionary. There is no way to find out how the word in the filename is associated with the short story mentioned inside. And that is fine. Because meaning is not inherent in this universe. It is perceived based on the knowledge of the observer.

    My understanding of this Universe will depend not only on my past expereinces, knowledge, age, cast, creed, location or n number of other parameters. But also something very unique to myself. 
    
    Something that this short story or the commit message where it's filename gets mentioned invokes within me.

    Based on these observations, You can see that this universe is built on few metaphorical rules:
        1. There is no meaning till a meaning is perceived.
        2. It cannot be controlled. Because to control you need to perceive the meaning of creator who created it without any meaning.
        3. It has unknown dimensions that we have not discovered yet.

    Any Questions?" - The researcher finished his presentation and waited for the questions from its audience.