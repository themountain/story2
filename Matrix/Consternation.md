    "Would you listen to me for a moment." said his father almost with an appeal in form of annoyance.

    "I have a time limit on this thing Father."

    "Have you looked at the water levels? There is no place to tie that rope." said the father.

    "But I have a time limit."

    "Where was your limit all these days?"

    "I can't control the weather father. But I need to get this done. Now, Will you teach me the knot or not?"

To his consternation, He left saddling the rope over his shoulders.

-- The End.