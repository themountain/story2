    “I could see the transformation of the whole generation.” 

    “How?” said the Secretary.

    “By just introducing a Tool, You could see a whole generation spending more and more time through Movies, Instagram reels, and TikTok.(He took a deep breath.)

    A whole generation just losing to a vice. But the worst part about it is, That we cannot do anything about it. Because we don’t understand it fully.

    What I can see for sure is that my Son is addicted.”

— The End.