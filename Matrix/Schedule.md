Looking at the notification on his phone, The professor said:

    "The more chronic anchors you keep in your environment, a schedule at which you do things. The better rooted you are in that reality."

    "Class dismissed!" roared the professor as he sprinted towards the classroom door.

The students took off their VR headsets being grateful to whoever sent a message to the professor.

-- The End.