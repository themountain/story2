    "Hey, Have you asked Joe if we can do that there?"

    "Yeah I have."

    "Because we will need this space to take the tractor inside."

    "There is a way to get the tractor from the other side. It is going to be fine."

    "Why are you in so much hurry? Wait and see what others are doing first."

    "I know you care. But I have a deadline. And I need to get this done." said the doer while Nitpicker scratched his head.

-- The End.