    "I have to come clean about this. I got addicted.

    Even after spending these 6 months in rehab, I still feel like I have not recovered." read the LinkedIn post.
    
-------* 7 years ago.

    "There are still 2 companies left." said my father on the phone.

    "Hmm" disheartned me replied in a low tone.

    "Don't worry about it. You will get the job."

    "You know, I won't get that much salary in off-campus hiring. Those 2 companies are my only chance."

    "Don't worry about salary. You know I used to earn 30Rs/month at my first job. You are definitely going to do better."

-------* Now.
    
    "You don't even notice that it is a substance to begin with. It gets introduced as a means of survival.
    
    Something that needs to be engaged with if you would like to be part of something bigger than yourself." continued the LinkedIn post.

-------* 2 years ago.

    "16 Lac per annum." said the HR representative on the other end of the call.

    "But I already have two offers worth 18 and 20 Lac."

    "Yeah, But are they working on such interesting probles as us?"

    "Yes. That is true." remembering my first chat with HR, I remembered my enthusiasm to work with this startup.

    
-------* Now.

    "Before you notice, You find yourself constantly searching for different means of getting more and more of it. You can see its effects reflected in your lifestyle. Your relatives and society expects you to have it on you.
    
    It distorts your long held beliefs so that you can justify your decisions on why you are degrading your values for it. You start ignoring the minor instances where it has started taking decisions on your behalf.

    Isn't that what addiction is after all? When the substance makes you do things that you would not if you were not exposed to it in the first place?" the LinkedIn post ends.

-------* 2 Days later.

    "Yo! That post was crazy. I got 100K likes on it. Here is your cut."

-- The End.    