As a rebellion, Diana started marking all the content suggested by the Algorithm as "inappropriate".

    "You don't have to show your true self online." said a reel on the Instagram feed.

    "WTF?" said Diana perplexed.

    "Matrix is a hoax." another reel said right after that one.

----------* 3 days later

    "Dianna is awful." - a friend shares a mockumentary on life of Diana.

    "Is anyone else experiencing this as well?" asked Diana to herself as she remembered her rebellion.

-- The End.
